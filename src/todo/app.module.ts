import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {environment} from '../environments/environment';
import { TodoComponent } from './todo/_todo-container/todo.component';
import { TodoCreationComponent } from './todo/todo-creation/todo-creation.component';
import { TodoFilterComponent } from './todo/todo-filter/todo-filter.component';
import { TodoListComponent } from './todo/todo-list/todo-list.component';
import { TodoStatusPipe } from './todo/_todo-container/pipe/todo-status.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodoCreationComponent,
    TodoFilterComponent,
    TodoListComponent,
    TodoStatusPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
