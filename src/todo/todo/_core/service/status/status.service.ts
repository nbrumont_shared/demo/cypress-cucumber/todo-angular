import {Injectable} from '@angular/core';
import {StatusQuery} from './store/status.query';
import {StatusStore} from './store/status.store';
import {Observable} from 'rxjs';
import {StatusEnum} from './status.enum';

@Injectable({providedIn: 'root'})
export class StatusService {
  constructor(
    private statusQuery: StatusQuery,
    private statusStore: StatusStore
  ) {}

  selectStatus(): Observable<StatusEnum> {
    return this.statusQuery.selectStatus();
  }

  setStatus(status: StatusEnum): void {
    this.statusStore.update({status});
  }
}
