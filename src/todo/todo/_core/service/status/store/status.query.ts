import {Query} from '@datorama/akita';
import {StatusEnum} from '../status.enum';
import {StatusStore} from './status.store';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class StatusQuery extends Query<{ status: StatusEnum }> {
  constructor(protected store: StatusStore) {
    super(store);
  }

  selectStatus(): Observable<StatusEnum> {
    return this.select().pipe(map(s => s.status));
  }
}
