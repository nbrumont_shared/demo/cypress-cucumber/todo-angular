import {Injectable} from '@angular/core';
import {Store, StoreConfig} from '@datorama/akita';
import {StatusEnum} from '../status.enum';

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'status'})
export class StatusStore extends Store<{status : StatusEnum}> {
  constructor() {
    super({status : StatusEnum.ALL});
  }
}
