import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Todo} from '../../../../_shared/model/todo.model';
import {Observable} from 'rxjs';
import {TodoForm} from '../../../../_shared/model/todo-form.model';

@Injectable({providedIn: 'root'})
export class TodoHttpService {
  constructor(private http: HttpClient) {}

  fetchTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>('/api/todos');
  }

  saveTodo(todo: TodoForm): Observable<Todo> {
    return this.http.post<Todo>('/api/todos', {label: todo.label});
  }

  updateTodo(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>('/api/todos', todo);
  }

  deleteTodo(id: number): Observable<void> {
    return this.http.delete<void>(`/api/todos/${id}`);
  }
}
