import {EntityState} from '@datorama/akita';
import {Todo} from '../../../../_shared/model/todo.model';

export interface TodoState extends EntityState<Todo> {}
