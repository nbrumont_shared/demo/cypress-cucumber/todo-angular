import {Injectable} from '@angular/core';
import {EntityStore, StoreConfig} from '@datorama/akita';
import {TodoState} from './todo.state';

@StoreConfig({name: 'todos'})
@Injectable({providedIn: 'root'})
export class TodoStore extends EntityStore<TodoState> {
  constructor() {
    super([]);
  }
}
