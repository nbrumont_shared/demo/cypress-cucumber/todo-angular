import {Injectable} from '@angular/core';
import {TodoQuery} from './store/todo.query';
import {TodoStore} from './store/todo.store';
import {Observable} from 'rxjs';
import {Todo} from '../../../_shared/model/todo.model';
import {TodoForm} from '../../../_shared/model/todo-form.model';
import {guid} from '@datorama/akita';
import {TodoHttpService} from './http/todo-http.service';

@Injectable({providedIn: 'root'})
export class TodoService {
  constructor(
    private todoQuery: TodoQuery,
    private todoStore: TodoStore,
    private todoHttpService: TodoHttpService
  ) {
    this.todoHttpService.fetchTodos().subscribe(
      todos => this.todoStore.set(todos)
    );
  }

  selectAll(): Observable<Todo[]> {
    return this.todoQuery.selectAll();
  }

  createTodo(todo: TodoForm): void {
    this.todoHttpService.saveTodo(todo).subscribe(
      newTodo => this.todoStore.add(newTodo)
    );
  }

  toggleTodo(todo: Todo): void {
    this.todoHttpService.updateTodo({...todo, completed: !todo.completed}).subscribe(
      newTodo => {
        this.todoStore.update(newTodo.id, {
          completed: newTodo.completed
        });
      }
    );
  }

  deleteTodo(id: number): void {
    this.todoHttpService.deleteTodo(id).subscribe(
      _ => {},
      e => {},
      () => this.todoStore.remove(id)
    );
  }
}
