import {Pipe, PipeTransform} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {Todo} from '../../_shared/model/todo.model';
import {StatusEnum} from '../../_core/service/status/status.enum';
import {map} from 'rxjs/operators';

@Pipe({
  name: 'todoStatus'
})
export class TodoStatusPipe implements PipeTransform {

  transform(todos$: Observable<Todo[]>, status$: Observable<StatusEnum>): Observable<Todo[]> {
    return combineLatest([todos$, status$]).pipe(
      map(([todos, stat]) => todos.filter(t => stat === StatusEnum.ALL ? true : stat === StatusEnum.COMPLETED ? t.completed : !t.completed
    )));
  }

}
