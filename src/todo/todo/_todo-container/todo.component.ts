import {Component, OnInit} from '@angular/core';
import {TodoService} from '../_core/service/todo/todo.service';
import {Observable} from 'rxjs';
import {Todo} from '../_shared/model/todo.model';
import {TodoForm} from '../_shared/model/todo-form.model';
import {StatusEnum} from '../_core/service/status/status.enum';
import {StatusService} from '../_core/service/status/status.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
})
export class TodoComponent implements OnInit {

  constructor(private todoService: TodoService, private statusService: StatusService) {
  }

  todos$: Observable<Todo[]> = this.todoService.selectAll();
  todosCount$: Observable<number> = this.todos$.pipe(map(todos => todos.filter(t => !t.completed).length));
  status$: Observable<StatusEnum> = this.statusService.selectStatus();

  ngOnInit() {
  }

  onAddTodo(todo: TodoForm): void {
    this.todoService.createTodo(todo);
  }

  onSelectStatus(statusEnum: StatusEnum): void {
    this.statusService.setStatus(statusEnum);
  }

  onToggle(todo: Todo): void {
    this.todoService.toggleTodo(todo);
  }

  onDelete(id: number) {
    this.todoService.deleteTodo(id);
  }
}
