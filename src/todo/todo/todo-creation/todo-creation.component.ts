import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {TodoForm} from '../_shared/model/todo-form.model';

@Component({
  selector: 'app-todo-creation',
  templateUrl: './todo-creation.component.html',
})
export class TodoCreationComponent implements OnInit {

  @Output() addTodo = new EventEmitter<TodoForm>();
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({label: ''});
  }

  onAddTodo(todo: TodoForm) {
    this.addTodo.emit(todo);
    this.form.reset();
  }
}
