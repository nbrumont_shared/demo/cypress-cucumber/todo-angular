import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StatusEnum} from '../_core/service/status/status.enum';

@Component({
  selector: 'app-todo-filter',
  templateUrl: './todo-filter.component.html',
})
export class TodoFilterComponent implements OnInit {

  @Input() todoCount: number;
  @Input() selectedStatut: StatusEnum;
  @Output() selectStatut = new EventEmitter<StatusEnum>();

  StatusEnum = StatusEnum;

  constructor() { }

  ngOnInit() {
  }


  setStatus(statusEnum: StatusEnum): void {
    this.selectStatut.emit(statusEnum);
  }

  isStatus(statusEnum: StatusEnum): boolean {
    return this.selectedStatut === statusEnum;
  }
}
