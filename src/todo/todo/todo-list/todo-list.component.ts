import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo} from '../_shared/model/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
})
export class TodoListComponent implements OnInit {

  @Input() todos: Todo[];
  @Output() toggle = new EventEmitter<Todo>();
  @Output() delete = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onToggle(todo: Todo): void {
    this.toggle.emit(todo);
  }

  onDelete(id: number) {
    this.delete.emit(id);
  }
}
